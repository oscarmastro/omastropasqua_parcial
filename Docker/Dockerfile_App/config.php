<?php

$g_hostname = '{MYSQL_DB_HOST}';
$g_db_type = 'mysqli';
$g_database_name = '{MYSQL_DB_DATABASE}';
$g_db_username = '{MYSQL_DB_USER}';
$g_db_password = '{MYSQL_DB_PASSWORD}';
$g_db_table_prefix = '';
$g_db_table_plugin_prefix = '';
$g_db_table_suffix = '';
$g_default_timezone = 'America/Argentina/Buenos_Aires';
$g_crypto_master_salt = 'B2VLK7xaZAQM3PlLn1Zs0wc+B/upzIW+e3VlkYjo0cI=';

// -----------------------------------------------------------------------------------------

// Mi base
// Database Config
// define ('DB_TYPE', 'mysqli');  // using PEAR::DB naming (mysql, pgsql, etc.)
// define ('DB_HOST', 'db'); // hostname of database server
// define ('DB_PORT', '3306'); // hostname of database server
// define ('DB_DATABASE', 'phpbugtrackerDB'); // database name
// define ('DB_USER', 'root'); // username for database connection
// define ('DB_PASSWORD', 'root'); // password for database connection

// -----------------------------------------------------------------------------------------

// Database Config
// define ('DB_TYPE', '{db_type}');  // using PEAR::DB naming (mysql, pgsql, etc.)
// define ('DB_HOST', '{db_host}'); // hostname of database server
// define ('DB_PORT', '{db_port}'); // hostname of database server
// define ('DB_DATABASE', '{db_database}'); // database name
// define ('DB_USER', '{db_user}'); // username for database connection
// define ('DB_PASSWORD', '{db_pass}'); // password for database connection


